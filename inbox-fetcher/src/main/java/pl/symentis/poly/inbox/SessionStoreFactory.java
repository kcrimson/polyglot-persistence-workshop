package pl.symentis.poly.inbox;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.springframework.stereotype.Component;

@Component
public class SessionStoreFactory {

	public Store connectToStore(String storeProtocol, String host, String user, String password)
			throws MessagingException {
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", storeProtocol);
		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(storeProtocol);
		store.connect(host, user, password);
		return store;
	}

}
