package pl.symentis.poly.inbox;

import javax.mail.Message;
import javax.mail.MessagingException;

public interface MessageFetcher {

	int getMessageCount() throws MessagingException;

	Message getMessage(int number) throws MessagingException;

}
