package pl.symentis.poly.inbox;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IMAPFolderMessageFetcherFactory {
	
	private final SessionStoreFactory sessionStoreFactory;
	
	@Autowired
	public IMAPFolderMessageFetcherFactory(SessionStoreFactory sessionStoreFactory) {
		this.sessionStoreFactory = sessionStoreFactory;
	}

	public MessageFetcher create(InboxRequest request) throws MessagingException {
		
		Store store = sessionStoreFactory.connectToStore(
				"imaps", 
				request.getHost(), 
				request.getUser(), 
				request.getPassword());
		
		Folder folder = store.getFolder("Inbox");
		folder.open(Folder.READ_ONLY);
		
		return new IMAPFolderMessageFetcher(folder);
	}

}
