package pl.symentis.poly.inbox;

import javax.mail.Message;
import javax.mail.MessagingException;

import pl.symentis.poly.inbox.MessageFetcher;

class MessageFetcherAdapter implements MessageFetcher {

	private final Message[] messages;

	MessageFetcherAdapter(Message[] messages) {
		super();
		this.messages = messages;
	}

	@Override
	public int getMessageCount() throws MessagingException {
		return messages.length;
	}

	@Override
	public Message getMessage(int number) throws MessagingException {
		return messages[number-1];
	}

}
