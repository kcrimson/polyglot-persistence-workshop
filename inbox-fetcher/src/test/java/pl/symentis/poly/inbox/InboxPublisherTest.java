package pl.symentis.poly.inbox;

import static com.google.common.collect.Queues.newArrayDeque;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import redis.embedded.RedisServer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Bootstrap.class)
@ActiveProfiles("test")
public class InboxPublisherTest {

	private static RedisServer redisServer;

	@Autowired
	private RedisConnectionFactory redisConnFcty;

	@Autowired
	private InboxPublisher inboxFetcher;

	private RedisMessageListenerContainer testContainer;

	@BeforeClass
	public static void startRedis() {
		BasicConfigurator.configure();

		redisServer = RedisServer.builder().port(6379).build();
		redisServer.start();

	}

	@AfterClass
	public static void stopRedis() {
		redisServer.stop();
	}

	@Before
	public void setupListenerContainer() {
		testContainer = new RedisMessageListenerContainer();
		testContainer.setConnectionFactory(redisConnFcty);
		testContainer.afterPropertiesSet();
		testContainer.start();
	}

	@After
	public void stopListenerContainer() throws Exception {
		testContainer.stop();
		testContainer.destroy();
	}

	@Test
	public void publish_valid_messages() throws IOException, InterruptedException, ExecutionException, TimeoutException {

		ArrayDeque<SettableFuture<Message>> futures = newArrayDeque(asList(
				SettableFuture.create(), 
				SettableFuture.create()));
		ListenableFuture<List<Message>> listOfFutures = Futures.allAsList(futures);

		testContainer.addMessageListener((message, bytes) -> {
			futures.poll().set(message);
		}, new ChannelTopic("mails"));

		MessageFetcherAdapter messageFetcherAdapter = new MessageFetcherAdapter(
				new javax.mail.Message[] { new MessageAdapter(), new MessageAdapter() });

		inboxFetcher.fetchInbox(messageFetcherAdapter);

		List<Message> actuals = listOfFutures.get();
		assertThat(actuals).hasSize(2);

	}

}
