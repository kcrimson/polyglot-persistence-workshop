package pl.symentis.poly.inbox;

import static java.util.Optional.ofNullable;

import java.io.UnsupportedEncodingException;
import java.util.stream.Stream;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;


public class MailMessage {

	private String subject;

	private String[] from;

	private String[] to;

	private String[] cc;

	private String[] bcc;

	private String date;

	private String messageId;

	private String inReplyTo;

	private String body;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String[] getFrom() {
		return from;
	}

	public void setFrom(String[] from) {
		this.from = from;
	}

	public String[] getCc() {
		return cc;
	}

	public void setCc(String[] cc) {
		this.cc = cc;
	}

	public String[] getBcc() {
		return bcc;
	}

	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getInReplyTo() {
		return inReplyTo;
	}

	public void setInReplyTo(String inReplyTo) {
		this.inReplyTo = inReplyTo;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public String[] getTo() {
		return to;
	}

	public void setTo(String[] to) {
		this.to = to;
	}

	public static MailMessage fromMessage(Message message) throws MessagingException, UnsupportedEncodingException{
		MailMessage mailMessage = new MailMessage();
		
		mailMessage.setFrom(
				Stream.of(ofNullable(message.getFrom()).orElse(new Address[]{}))
					.filter(address -> address instanceof InternetAddress)
					.map(address -> (InternetAddress)address )
					.map(InternetAddress::getAddress)
					.toArray(size -> new String[size]));
		
		mailMessage.setTo(
				Stream.of(ofNullable(message.getRecipients(RecipientType.TO)).orElse(new Address[]{}))
					.filter(address -> address instanceof InternetAddress)
					.map(address -> (InternetAddress)address )
					.map(InternetAddress::getAddress)
					.toArray(size -> new String[size]));
		
		mailMessage.setSubject(MimeUtility.decodeText(message.getSubject()));
		
		return mailMessage;
	}
}
