package pl.symentis.poly.inbox;

import java.util.concurrent.atomic.AtomicInteger;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javaslang.control.Try;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

@Component
public class InboxPublisher {

	private static final Logger LOGGER = LoggerFactory.getLogger(InboxPublisher.class);

	private final RedisTemplate<String, Object> redisTemplate;

	@Autowired
	public InboxPublisher(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public void fetchInbox(MessageFetcher folderFetcher) {
		Observable.<Message> create((subscriber) -> {
				AtomicInteger fetchedMessages = new AtomicInteger(0);
				subscriber.setProducer(n -> {
					// HINT: http://akarnokd.blogspot.com/2015/05/operator-concurrency-primitives_12.html
					int countOfFetchedMsg=0;
					for(;;){
						int msgNr = fetchedMessages.incrementAndGet();
						try {
							if(msgNr>folderFetcher.getMessageCount()){
								// there are no more messages in folder
								subscriber.onCompleted();
								return;
							}
							subscriber.onNext(folderFetcher.getMessage(msgNr));
							countOfFetchedMsg++;
							if(countOfFetchedMsg>=n){
								// we have fetched requested number of messages
								return;
							}
						} catch (MessagingException e) {
							subscriber.onError(e);
						}
					}
					
					
				});
		})
		.observeOn(Schedulers.io())
		.map(this::mapMessage)
		.flatMap(t -> t.map(Observable::just).getOrElseGet(InboxPublisher::error))
		.subscribeOn(Schedulers.io())
		.subscribe(new Subscriber<MailMessage>() {

			@Override
			public void onCompleted() {
				LOGGER.debug("finished fetching inbox");
			}

			@Override
			public void onError(Throwable e) {
				LOGGER.error("fatalilty during inbox fetching", e);
			}

			@Override
			public void onNext(MailMessage t) {
				redisTemplate.convertAndSend("mails", t);
				request(1);
			}
		});

	}

	private Try<MailMessage> mapMessage(Message message) {
		return Try.of( () -> MailMessage.fromMessage(message));
	}
	
	private static <T> Observable<T> error(Throwable e){
		return Observable.create(subscriber -> subscriber.onError(e));
	}

}
