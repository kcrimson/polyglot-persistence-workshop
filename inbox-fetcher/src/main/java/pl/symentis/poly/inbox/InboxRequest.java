package pl.symentis.poly.inbox;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InboxRequest {

	private String host;
	private String user;
	private String password;
	private String[] folders;

	public InboxRequest(
			@JsonProperty("host") String host, 
			@JsonProperty("user")String user, 
			@JsonProperty("password")String password, 
			@JsonProperty("folders")String[] folders) {
		super();
		this.host = host;
		this.user = user;
		this.password = password;
		this.folders = folders;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String[] getFolders() {
		return folders;
	}

	public void setFolders(String[] folders) {
		this.folders = folders;
	}

}
