package pl.symentis.poly.inbox;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class RESTEndpoints extends ResourceConfig{

	public RESTEndpoints() {
		register(InboxEndpoint.class);
	}


	
}
