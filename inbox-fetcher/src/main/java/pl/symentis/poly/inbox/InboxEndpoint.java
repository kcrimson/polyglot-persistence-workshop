package pl.symentis.poly.inbox;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Path("/inbox")
@Component
public class InboxEndpoint {
	
	private final InboxPublisher publisher;
	private final IMAPFolderMessageFetcherFactory messageFetcherFactory;
	
	@Autowired
	public InboxEndpoint(InboxPublisher publisher, IMAPFolderMessageFetcherFactory messageFetcherFactory) {
		this.publisher = publisher;
		this.messageFetcherFactory = messageFetcherFactory;
	}

	@PUT
	@Consumes("application/json")
	public void fetchInbox(InboxRequest request) throws MessagingException{
		MessageFetcher messageFetcher = messageFetcherFactory.create(request);
		publisher.fetchInbox(messageFetcher);
	}
}
