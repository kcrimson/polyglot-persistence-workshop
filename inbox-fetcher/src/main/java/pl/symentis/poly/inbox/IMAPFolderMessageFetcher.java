package pl.symentis.poly.inbox;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;

public class IMAPFolderMessageFetcher implements MessageFetcher{

	private final Folder folder;

	public IMAPFolderMessageFetcher(Folder folder) {
		this.folder = folder;
	}

	@Override
	public Message getMessage(int number) throws MessagingException {
		return folder.getMessage(number);
	}
	
	@Override
	public int getMessageCount() throws MessagingException{
		return folder.getMessageCount();
	}
}
